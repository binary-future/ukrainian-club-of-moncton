import Head from 'next/head'
import { Contacts } from '../components/sections/about/Contacts'
import { Informative } from '../components/sections/about/Informative'
import { AboutFront } from './../components/sections/about/AboutFront'

import { useRouter } from 'next/router'
import { getAllText } from './../services/getTexts.js'

export default function About() {
  const router = useRouter()
  
  const { data, loading, error, loadingFrom100 , errorFrom100} = getAllText(router.locale);

  if (loading || loadingFrom100) return <span></span>
  if (error || errorFrom100) return <span></span>

  return (
    <>
    <Head>
      <title>About - {data['assosiation-of-moncton']}</title>
      <meta
        name="description"
        content={data["about-description-seo"]}
      />

      <link
        rel="icon"
        href="/favicon.ico"
      />
    </Head>
    
      
    <AboutFront />
    <main id='main'>
      <div className='about-section'>
        <div className='about-section-bg'>
          <Informative />
          <Contacts />
        </div>
      </div>
    </main>
    </>
  )
}
